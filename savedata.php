<?php
	$method = $_SERVER['REQUEST_METHOD'];
	$currentVersion = 0;
	$database_dir = './database';
	if ($handle = opendir($database_dir)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				$version = str_replace(".json","",$entry);
				if ($version > $currentVersion) {
					$currentVersion = $version;
				}
			}
		}
		closedir($handle);
	} else {
		exit("cannot open directory");
	}
	if ($method === 'GET') {
		echo file_get_contents("$database_dir/$currentVersion.json");
		exit(0);
	}

	if ($method !== 'POST') {
		exit('Invalid method');
	}

	$data = file_get_contents('php://input');
	$json = json_decode($data);
	if (!$json) {
		exit("unable to parse json ".json_last_error());
	}

	$saveVersion = intval($json->{'version'});
	echo "Saved version: ".$saveVersion."\n";
	if ($currentVersion >= $saveVersion) {
		exit("Conflict. Update first");
	}

	$database = json_encode($json, JSON_PRETTY_PRINT);

	$myfile = fopen("$database_dir/$saveVersion.json", "w") or exit("Unable to open file!");
	fwrite($myfile, $database);
	fclose($myfile);
	exit('OK');
?>
