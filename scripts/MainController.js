(function() {
	'use strict';
	
	angular.module('pimpadb').controller('MainController', ['$http', 'fieldsService', 'persistenceService',
	function MainController($http, fieldsService, persistenceService) {

		var vm = this;

		vm.schema_version = 2;
		vm.model = {};
		vm.betoltes_model = {};
		vm.fields = fieldsService.getFields(vm.schema_version);

		vm.betoltes_fields = [
			{
				className: 'section-label',
				template: '<div><h3>Törzslap betöltése</h3></div>'
			},
			{
				key: 'torzsszam',
				type: 'input',
				templateOptions: {
					type: 'text',
					label: 'Törzsszám/naplószám',
					maxlength: 20,
					required: true
				}
			},
		];

		vm.loadData = function() {
			var	szam = vm.betoltes_model.torzsszam;
			persistenceService.loadItems().then(function(items) {
				var item = items[szam];
				if (item) {
					vm.model = item;
					toastr.success(szam + ' törzslap betöltve');
				} else if (!item) {
					var ans = confirm('Még nincs ilyen azonosítójú törzslap. Készítsünk egy újat?');
					if (!ans) {
						return;
					}
					vm.model = {
						schema_version: vm.schema_version,
						data_version: 1,
						alapadatok: {
							torzsszam: szam,
						},
						anamnezis: {
							zavartalan_perinatalis_adaptacio: true
						}
					}
					toastr.success(szam + ' törzslap elkészítve');
				}
			}).catch(function(e) {
				toastr.error(szam + ' törzslap betöltése sikertelen');
				console.log('Betöltés hiba: ', e);
			});
		}

		vm.saveData = function() {
			var szam = vm.model.alapadatok.torzsszam;

			persistenceService
			.saveItem(szam, vm.model)
			.then(function(newItems) {
				vm.model = newItems[szam];
				toastr.success(szam + ' törzslap mentése sikerült');
			}).catch(function(e) {
				if ((typeof e) == 'string') {
					toastr.error(e);
				} else {
					toastr.error(szam + ' törzslap mentése nem sikerült');
				}
				console.log('Mentés hiba: ', e);
			});
		}

		vm.delete = function() {
			var szam = vm.model.alapadatok.torzsszam;
			var ans = confirm('Biztosan törli a törzslapot minden adatával együtt?');
			if (!ans) {
				return;
			}

			persistenceService
			.deleteItem(szam, vm.model)
			.then(function(newItems) {
				vm.model = {};
				toastr.success(szam + ' törzslap törölve');
			}).catch(function(e) {
				if ((typeof e) == 'string') {
					toastr.error(e);
				} else {
					toastr.error(szam + ' törzslap törlése nem sikerült');
				}
				console.log('Törlés hiba: ', e);
			});
		}

		vm.export = function() {
			persistenceService.loadItems()
							  .then(function(items) {
				var wb = new Workbook();
				var worksheet = XLSX.utils.aoa_to_sheet(fieldsService.modelToArrayOfArrays(items));

				wb.SheetNames.push('Bronchiolitis');
				wb.Sheets['Bronchiolitis'] = worksheet;
				var out = XLSX.write(wb, {bookType: 'xlsx', bookSST: true, type: 'binary'});
				saveAs(new Blob([s2ab(out)], {type: 'application/octet-stream'}), "bronchiolitis.xlsx");
			});
		}

		function Workbook() {
			if (!(this instanceof Workbook)) 
				return new Workbook();
			this.SheetNames = [];
			this.Sheets = {};
		}

		function s2ab(s) {
			var buf = new ArrayBuffer(s.length);
			var view = new Uint8Array(buf);
			for (var i=0; i!=s.length; ++i) 
				view[i] = s.charCodeAt(i) & 0xFF;
			return buf;
		}

	}])
	.directive("myClick", ['$parse', function($parse) {
		return {
			restrict: "A",
			link: function(scope, elem, attrs) {
				var fn = $parse(attrs['myClick'], null, true);
				elem[0].addEventListener('click', function(event) {
					scope.$apply(fn);
				});
			}
		}
	}])

})();
