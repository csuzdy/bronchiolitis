(function() {
    'use strict';

    angular.module('pimpadb').factory('persistenceService', ['$http', '$q', 
	function persistenceService($http, $q) {

		function loadItems() {
			return _loadItems();
		}

		function saveItem(szam, newItem) {
			return loadItems().then(function(items) {
				if (items[szam] && items[szam].data_version > newItem.data_version) {
					console.warn('Local: ', newItem.data_version, 'Remote: ', items[szam].data_version);
					return $q.reject('A ' + szam + ' törzslapot időközben már módosították, kérem töltse be újra!');
				}
				newItem.data_version += 1;
				items[szam] = newItem;
				return _storeItems(items);
			});
		}

		function deleteItem(szam, removeableItem) {
			return loadItems().then(function(items) {
				if (items[szam] && items[szam].data_version > removeableItem.data_version) {
					console.warn('Local: ', removeableItem.data_version, 'Remote: ', items[szam].data_version);
					return $q.reject('A ' + szam + ' törzslapot időközben már módosították, kérem töltse be újra!');
				}
				delete items[szam];
				return _storeItems(items);
			});
		}

		function _loadItems() {
			return $http({
					method: 'GET',
					url: '/savedata.php'
				}).then(function successCallback(response) {
					console.log('Sikerült letölteni az adatokat a szerverrről.');
					return response.data;
				}, function errorCallback(response) {
					console.warn('Nem sikerült letölteni az adatokat a szerverről.');
					return $q.reject('Nem siketült letölteni az adatokat a szerverről');
				});
		}

		function _storeItems(items) {
			items['version'] = items['version'] || 0;
			items['version'] += 1;
			var itemsStr = JSON.stringify(items);
			console.log('Adat küldés a szervernek: ', itemsStr);
			return $http.post('/savedata.php',  itemsStr)
						.then(function successCallback(response) {
					var data = response.data;
					console.log('Szerver válasz megérkezett: ', data);
					if (!data || data.indexOf('OK') == -1) {
						return $q.reject('Nem sikerült az adatok mentése, töltse újra a törzslapot!');
					}
					return items;
				}, function errorCallback(response) {
					console.warn('Nem sikerült az adatok mentése, töltse újra a törzslapot!');
					return $q.reject('Nem siketült az adatok mentése, töltse újra a törzslapot!');
				});
		}

		return {
			loadItems: loadItems,
			saveItem: saveItem,
			deleteItem: deleteItem
		};
	}]);

})();
