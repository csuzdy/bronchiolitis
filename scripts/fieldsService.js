(function() {
	'use strict';

	angular.module('pimpadb').factory('service', service)
							 .factory('fieldsService', ['service', '$parse', fieldsService]);
	function service() {
	
		function getNemItems() {
			return [
				{
					"name" : "fiú",
					"value" : "1"
				},
				{
					"name" : "lány",
					"value" : "2"
				}
			];
		}

		function getAnyatejItems() {
			return [
				{
					"name" : "kizárólag",
					"value" : "2"
				},
				{
					"name" : "részben",
					"value" : "1"
				},
				{
					"name" : "nem",
					"value" : "0"
				}
			];
		}

		function getSzortyzorejItems() {
			return [
				{
					"name" : "nincs",
					"value" : "0"
				},
				{
					"name" : "egyoldali",
					"value" : "1"
				},
				{
					"name" : "kétoldali",
					"value" : "2"
				}
			];
		}

		function getRSVItems() {
			return [
				{
					"name" : "negatív",
					"value" : "0"
				},
				{
					"name" : "pozitív",
					"value" : "1"
				},
			];
		}

		function getInfusioItems() {
			return [
				{
					"name" : "1",
					"value" : "1"
				},
				{
					"name" : "több",
					"value" : "2"
				},
			];
		}

		function getIgenNemItems() {
			return [
				{
					"name" : "igen",
					"value" : "1"
				},
				{
					"name" : "nem",
					"value" : "0"
				},
			];
		}	

		return {
			getNemItems : getNemItems,
			getAnyatejItems: getAnyatejItems,
			getSzortyzorejItems: getSzortyzorejItems,
			getRSVItems: getRSVItems,
			getIgenNemItems: getIgenNemItems,
			getInfusioItems: getInfusioItems
		}
	}

	function fieldsService(service, $parse) {

		function getFields(schema_version) {
			if (!schema_version) {
				throw 'no version specified';
			}
			switch (schema_version) {
				case 2:
					return getFields2();
				default:
					throw 'invalid data structure version';
			}
		}

		function getFields2() {
			var fields = [
				{
					className: 'section-label',
					template: '<div><h3>Alapadatok</h3></div>'
				},
				{
					key: 'schema_version',
					type: 'input',
					templateOptions: {
						type: 'hidden',
						required: true
					}
				},
				{
					key: 'data_version',
					type: 'input',
					templateOptions: {
						type: 'hidden',
						required: true
					}
				},
				{
					key: 'alapadatok.torzsszam',
					type: 'input',
					templateOptions: {
						disabled: true,
						type: 'text',
						label: 'Törzsszám/naplószám',
						required: true
					}
				},
				{
					key: 'alapadatok.nem',
					type: 'select',
					templateOptions: {
						label: 'Nem',
						required: true,
						options: service.getNemItems()
					}
				},
				{
					key: 'alapadatok.eletkor_felvetelkor',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Ételkor felvételkor',
						maxlength: 4,
						max: 12.5,
						min: 0.5,
						step: 0.5,
						addonRight: {
							text: 'hónap'
						},
					}
				},
				{
					key: 'alapadatok.korhazban_toltott_napok',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Kórházban töltött napok száma',
						maxlength: 3,
						min: 1,
						max: 100,
						addonRight: {
							text: 'nap'
						},
					}
				},
				{
					className: 'section-label',
					template: '<div><h3>Anamnézis</h3></div>'
				},
				{
					key: 'anamnezis.gestatios_het',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Gestatios hét',
						maxlength: 2,
						min: 23,
						max: 42,
						addonRight: {
							text: 'hét'
						},
					}
				},
				{
					key: 'anamnezis.szuletesi_suly',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Születési súly',
						maxlength: 4,
						min: 400,
						max: 6000,
						addonRight: {
							text: 'gramm'
						},
					}
				},
				{
					key: 'anamnezis.zavartalan_perinatalis_adaptacio',
					type: 'checkbox',
					defaultValue: true,
					templateOptions: {
						label: 'Zavartalan perinatális adaptáció?',
					}
				},
				{
					hideExpression: 'model.anamnezis.zavartalan_perinatalis_adaptacio',
					fieldGroup: [
						{
							className: 'col-xs-6 col-sm-2 col-lg-3',
							key: 'anamnezis.nCAP',
							type: 'checkbox',
							templateOptions: {
								label: 'nCAP'
							}
						},
						{
							className: 'col-xs-6 col-sm-2 col-lg-3',
							key: 'anamnezis.SIMV',
							type: 'checkbox',
							templateOptions: {
								label: 'SIMV'
							}
						},
						{
							className: 'col-xs-6 col-sm-1 col-lg-3',
							key: 'anamnezis.O2',
							type: 'checkbox',
							templateOptions: {
								label: 'O2'
							}
						},
						{
							className: 'col-xs-6 col-sm-3 col-lg-3',
							key: 'anamnezis.surfactant',
							type: 'checkbox',
							templateOptions: {
								label: 'surfactant '
							}
						},
						{
							className: 'col-xs-12 col-sm-4 col-lg-12',
							key: 'anamnezis.keringestamogatas',
							type: 'checkbox',
							templateOptions: {
								label: 'keringéstámogatás'
							}
						},
					]			
				},
				{
					key: 'anamnezis.perinatalis_megjegyzes',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Megjegyzés',
						maxlength: 255
					}
				},
				{
					template: '<hr/>'
				},
				{
					key: 'anamnezis.kardiopulmonalis_alapbetegseg',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Kardiopulmonalis alapbetegség',
						maxlength: 255
					}
				},
				{
					key: 'anamnezis.immundeficiencia',
					type: 'checkbox',
					templateOptions: {
						label: 'Immundeficiencia',
					}
				},
				{
					key: 'anamnezis.RSV_immunizacio',
					type: 'select',
					templateOptions: {
						label: 'RSV immunizáció',
						options: service.getIgenNemItems()
					}
				},
				{
					key: 'anamnezis.passziv_dohanyzas',
					type: 'select',
					templateOptions: {
						label: 'Passzív dohányzás',
						options: service.getIgenNemItems()
					}
				},
				{
					key: 'anamnezis.anyatej',
					type: 'select',
					templateOptions: {
						label: 'Anyatej',
						options: service.getAnyatejItems()
					}
				},
				{
					className: 'section-label',
					template: '<div><h3>Fizikális státusz</h3></div>'
				},
				{
					key: 'fizikalis.legzesszam',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Légzésszám',
						maxlength: 3,
						min: 10,
						max: 120,
						addonRight: {
							text: '/ perc'
						},
					}
				},
				{
					key: 'fizikalis.pulzus',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Pulzus',
						maxlength: 3,
						min: 60,
						max: 300,
						addonRight: {
							text: '/ perc'
						},
					}
				},
				{
					key: 'fizikalis.szaturacio',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Szaturáció',
						maxlength: 3,
						min: 50,
						max: 100,
						addonRight: {
							text: '%'
						},
					}
				},
				{
					key: 'fizikalis.crt',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'CRT',
						maxlength: 1,
						min: 1,
						max: 6,
						addonRight: {
							text: 'sec'
						},
					}
				},
				{
					fieldGroup: [
						{
							className: 'col-xs-12 col-sm-6',
							key: 'fizikalis.exsiccatio',
							type: 'checkbox',
							templateOptions: {
								label: 'Exsiccatio',
							}
						},
						{
							className: 'col-xs-12 col-sm-6',
							key: 'fizikalis.dyspnoes_jelek',
							type: 'checkbox',
							templateOptions: {
								label: 'Dyspnoés jelek',
							}
						},
						{
							className: 'col-xs-12 col-sm-6',
							key: 'fizikalis.wheezing',
							type: 'checkbox',
							templateOptions: {
								label: 'Wheezing',
							}
						},
						{
							className: 'col-xs-12 col-sm-6',
							key: 'fizikalis.cyanosis',
							type: 'checkbox',
							templateOptions: {
								label: 'Cyanosis',
							}
						}
					]
				},
				{
					key: 'fizikalis.szortyzorej',
					type: 'select',
					templateOptions: {
						label: 'Szörtyzörej',
						options: service.getSzortyzorejItems()
					}
				},
				{
					key: 'fizikalis.testhomerseklet_erkezeskor',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Testhőmérséklet érkezéskor (axilláris)',
						maxlength: 4,
						min: 30,
						max: 43,
						step: 0.1,
						addonRight: {
							text: '°C'
						},
					}
				},
				{
					key: 'fizikalis.laz',
					type: 'checkbox',
					templateOptions: {
						label: 'Láz',
					}
				},
				{
					hideExpression: '!model.fizikalis.laz',
					key: 'fizikalis.lazas_napok',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Lázas napok száma',
						maxlength: 3,
						min: 1,
						max: 100,
						required: true,
					},
				},
				{
					key: 'fizikalis.testtomeg_felvetelkor',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Testtömeg felvételkor',
						maxlength: 5,
						min: 1,
						max: 20,
						step: 0.01,
						addonRight: {
							text: 'kg'
						},
					},
				},
				{
					className: 'section-label',
					template: '<div><h3>Diagnosztika</h3></div>'
				},
				{
					key: 'diagnosztika.mrtg',
					type: 'checkbox',
					templateOptions: {
						label: 'MRTG',
					}
				},
				{
					hideExpression: '!model.diagnosztika.mrtg',
					key: 'diagnosztika.mrtg_elteres',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Eltérés',
						maxlength: 255,
					},
				},
				{
					key: 'diagnosztika.verkep',
					type: 'checkbox',
					templateOptions: {
						label: 'Vérkép',
					}
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.fvs',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Fehérvérsejtszám',
						min: 0,
						max: 100,
						step: 0.01,
						maxlength: 5,
						addonRight: {
							text: 'G/l'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.neu',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Neu',
						min: 0,
						max: 99,
						step: 0.01,
						maxlength: 5,
						addonRight: {
							text: '%'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.ly',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Ly',
						min: 0,
						max: 99,
						step: 0.01,
						maxlength: 5,
						addonRight: {
							text: '%'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.mono',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Mono',
						min: 0,
						max: 99,
						step: 0.01,
						maxlength: 5,
						addonRight: {
							text: '%'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.vvt',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Vörösvértestszám',
						min: 0,
						max: 100,
						step: 0.01,
						maxlength: 5,
						addonRight: {
							text: 'G/l'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.hgb',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Hgb',
						min: 0,
						max: 300,
						maxlength: 3,
						addonRight: {
							text: 'g/l'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.verkep',
					key: 'diagnosztika.htcr',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Htcr',
						min: 0,
						max: 1,
						step: 0.01,
						maxlength: 4,
					},
				},
				{
					key: 'diagnosztika.crp',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'CRP',
						min: 0,
						max: 400,
						step: 0.01,
						maxlength: 6,
						addonRight: {
							text: 'mg/l'
						},
					},
				},
				{
					key: 'diagnosztika.na',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Na',
						min: 50,
						max: 200,
						maxlength: 3,
						addonRight: {
							text: 'mmol/l'
						},
					},
				},
				{
					key: 'diagnosztika.k',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'K',
						min: 1,
						max: 10,
						step: 0.1,
						maxlength: 4,
						addonRight: {
							text: 'mmol/l'
						},
					},
				},
				{
					key: 'diagnosztika.cl',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Cl',
						min: 10,
						max: 200,
						maxlength: 3,
						addonRight: {
							text: 'mmol/l'
						},
					},
				},
				{
					key: 'diagnosztika.astrup',
					type: 'checkbox',
					templateOptions: {
						label: 'Astrup',
					}
				},
				{
					hideExpression: '!model.diagnosztika.astrup',
					key: 'diagnosztika.po2',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'pO2',
						min: 10,
						max: 200,
						maxlength: 3,
						addonRight: {
							text: 'Hgmm'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.astrup',
					key: 'diagnosztika.pco2',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'pCO2',
						min: 10,
						max: 200,
						maxlength: 3,
						addonRight: {
							text: 'Hgmm'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.astrup',
					key: 'diagnosztika.ph',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'pH',
						min: 6,
						max: 8,
						step: 0.01,
						maxlength: 4,
					},
				},
				{
					hideExpression: '!model.diagnosztika.astrup',
					key: 'diagnosztika.laktat',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Laktát',
						min: 0,
						max: 10,
						step: 0.1,
						maxlength: 4,
						addonRight: {
							text: 'mmol/l'
						},
					},
				},
				{
					hideExpression: '!model.diagnosztika.astrup',
					key: 'diagnosztika.be',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'BE',
						min: -50,
						max: 50,
						step: 0.1,
						maxlength: 5,
						addonRight: {
							text: 'mmol/l'
						},
					},
				},
				{
					key: 'diagnosztika.rsv',
					type: 'select',
					templateOptions: {
						label: 'RSV gyorsteszt',
						options: service.getRSVItems()
					}
				},
				{
					className: 'section-label',
					template: '<div><h3>Terápia</h3></div>'
				},
				{
					template: '<div><h4>Felvétel előtt</h3></div>'
				},
				{
					key: 'terapia.antibiotikum_fevetel_elott',
					type: 'checkbox',
					templateOptions: {
						label: 'Antibiotikum',
					}
				},
				{
					key: 'terapia.horgotagito_felvetel_elott',
					type: 'checkbox',
					templateOptions: {
						label: 'Hörgőtágító',
					}
				},
				{
					key: 'terapia.valadekoldo_felvetel_elott',
					type: 'checkbox',
					templateOptions: {
						label: 'Váladékoldó',
					}
				},
				{
					key: 'terapia.steroid_felvetel_elott',
					type: 'checkbox',
					templateOptions: {
						label: 'Steroid',
					}
				},
				{
					key: 'terapia.egyeb_felvetel_elott',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Egyéb',
						maxlength: 255,
					},
				},
				{
					template: '<div><h4>HPK</h3></div>'
				},
				{
					key: 'terapia.antibiotikum',
					type: 'checkbox',
					templateOptions: {
						label: 'Antibiotikum',
					}
				},
				{
					hideExpression: '!model.terapia.antibiotikum',
					key: 'terapia.antibiotikum_nev',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Antibiotikum neve',
						maxlength: 255,
						required: true,
					},
				},
				{
					key: 'terapia.salbutamol',
					type: 'checkbox',
					templateOptions: {
						label: 'Salbutamol',
					}
				},
				{
					hideExpression: '!model.terapia.salbutamol',
					key: 'terapia.salbutamol_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Salbutamol hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					key: 'terapia.so_inhal',
					type: 'checkbox',
					templateOptions: {
						label: '3 %-os só inhaláció',
					}
				},
				{
					hideExpression: '!model.terapia.so_inhal',
					key: 'terapia.so_inhal_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: '3 %-os só inhaláció hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					key: 'terapia.steroid',
					type: 'checkbox',
					templateOptions: {
						label: 'Steroid',
					}
				},
				{
					hideExpression: '!model.terapia.steroid',
					key: 'terapia.steroid_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'Steroid hány alkalommal',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'alkalom'
						},
					},
				},
				{
					key: 'terapia.o2',
					type: 'checkbox',
					templateOptions: {
						label: 'O2',
					}
				},
				{
					hideExpression: '!model.terapia.o2',
					key: 'terapia.o2_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'O2 hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					key: 'terapia.infusio',
					type: 'checkbox',
					templateOptions: {
						label: 'Infusio',
					}
				},
				{
					hideExpression: '!model.terapia.infusio',
					key: 'terapia.infusio_nap',
					type: 'select',
					templateOptions: {
						label: 'Infusio hány napig',
						required: true,
						options: service.getInfusioItems()
					}
				},
				{
					key: 'terapia.egyeb',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Egyéb',
						maxlength: 255,
					},
				},
				{
					className: 'section-label',
					template: '<div><h3>ITO</h3></div>'
				},
				{
					key: 'ito.ito',
					type: 'checkbox',
					templateOptions: {
						label: 'ITO',
					}
				},
				{
					hideExpression: '!model.ito.ito',
					key: 'ito.ito_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'ITO hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					hideExpression: '!model.ito.ito',
					key: 'ito.simv',
					type: 'checkbox',
					templateOptions: {
						label: 'SIMV',
					}
				},
				{
					hideExpression: '!model.ito.ito || !model.ito.simv',
					key: 'ito.simv_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'SIMV hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					hideExpression: '!model.ito.ito',
					key: 'ito.ncpap',
					type: 'checkbox',
					templateOptions: {
						label: 'nCPAP',
					}
				},
				{
					hideExpression: '!model.ito.ito || !model.ito.ncpap',
					key: 'ito.ncpap_nap',
					type: 'input',
					templateOptions: {
						type: 'number',
						label: 'nCPAP hány napig',
						min: 1,
						max: 100,
						maxlength: 3,
						required: true,
						addonRight: {
							text: 'nap'
						},
					},
				},
				{
					template: '<hr style="background-color: black;"/>'
				},
				{
					key: 'megjegyzes',
					type: 'input',
					templateOptions: {
						type: 'text',
						label: 'Általános megjegyzés',
						maxlength: 255,
					},
				},
			];

			return fields;
		}

		function modelToArrayOfArrays(model) {
			var fields = getFields(2);
			var sheet = [];
			var header = getExcelHeadersFromFields(fields);
			sheet.push(header);
			for (var i in model) {
				if (i == 'version') {
					continue;
				}
				var row = extractRow(model[i], header);
				sheet.push(row);
			}
			return sheet;
		}

		function getExcelHeadersFromFields(fields) {
			var header = [];
			fields.forEach(function(field) {
				var key = field['key'];
				if (key) {
					header.push(key);
				} else if (field['fieldGroup']) {
					var fieldGroup = field['fieldGroup'];
					fieldGroup.forEach(function(fieldGroupItem) {
						var key = fieldGroupItem['key'];
						if (key) {
							header.push(key);
						}
					});
				}
			});
			return header;
		}
		
		function extractRow(data, header) {
			var row = [];
			header.forEach(function(colName) {
				var d = $parse(colName)(data);
				row.push(d);
			});
			return row;
		}

		return {
			getFields : getFields,
			modelToArrayOfArrays: modelToArrayOfArrays 
		}
	}

})();
