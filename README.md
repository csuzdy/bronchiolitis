# bronchiolitis
Collecting data for medical research of bronchiolitis. (with [Angular formly](http://angular-formly.com/#/))

## Run it with Docker
```docker
docker build -t csuzdy:bronchiolitis .
docker run --rm -it -p 80:80 csuzdy:bronchiolitis
```
Open [http://localhost/index.html](http://localhost/index.html).
